package Ro.Orange;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class Main {

    public static void main(String[] args) throws IOException {
	    //variabila de tip string care tine numele fisierului.
        String fileName = "C:\\JavaFiles\\test.txt";

        //un obiect nou de tip filereader care citeste fisierul
        Reader fr = new FileReader(fileName);

        //invelim fr in bufferedreader care are metoda readLine si poate citi fisierul linie cu linie.
        BufferedReader br = new BufferedReader(fr);

        //intr-un try - catch incerc sa stochez linia citita de br intr-o variabila de tip string
        //si sa o afisez in consola. orice eroare din clasa IO va fi afisata pe ecran
        try {
            String line;
            while ((line = br.readLine())!=null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println("Error");
        //dupa ce am mai citit, se pare ca recomandarea este sa inchizi wrapper-ul intr-un finally
        } finally {
            br.close();
        }

        //inchid robinetul :)
        //fr.close();

    }
}
